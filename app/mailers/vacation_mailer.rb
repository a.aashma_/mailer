class VacationMailer < ApplicationMailer
    default from: 'aashmadahal@bajratechnologies.com'
  
    def vacation_mailer
        @url  = 'http://example.com/login'
        mail(to: "dahalaashma1@gmail.com", subject: 'Happy Holidays!')
    end

end
